object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 303
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 95
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 81
    Height = 180
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object Button2: TButton
    Left = 95
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 95
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 95
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Lower Case'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 95
    Top = 132
    Width = 75
    Height = 25
    Caption = 'Upper Case'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 95
    Top = 163
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 6
    OnClick = Button6Click
  end
  object Memo2: TMemo
    Left = 176
    Top = 8
    Width = 81
    Height = 180
    Lines.Strings = (
      'Memo2')
    TabOrder = 7
  end
end
